# Docker Php-fpm nginx postgres webmail configuration

This repository contains some services to start a new php project.

## Requirements

You should have:

- Docker
- Docker Composer
- Make utilities

## Installation

| **Tools**      | **Version**          |
|----------------|----------------------|
| **PHP-Apache** | 8.3.6                |
| **COMPOSER**   | 2.7.4                |
| **SYMFONY**    | 5.4.13               |
| **XDEBUG**     | 3.3.2                |
| **POSTGRESQL** | PostgreSQL 16.1      |
| **MAILER**     | Mail catcher (0.8.0) |

Firstly, you have to clone the repository:

```
> git clone https://gitlab.com/richy-cleancoders/docker-xdebug-composer-symfony-php-apache-postgresql-mailer.git
> cd docker-xdebug-composer-symfony-php-apache-postgresql-mailer
```

All docker configurations are available in the `docker` folder.

- `apache`: in `docker/apache` folder contains apache configuration

If you want to modify your application root directory, you have to change `root` option into `docker/apache/vhosts/default.conf` file

```
<VirtualHost *:80>
    ServerName localhost
    DocumentRoot /var/www/html/public
    DirectoryIndex /index.php

    <Directory /var/www/html/public>
        AllowOverride All
        Require all granted
        Allow from All

        FallbackResource /index.php
    </Directory>

    <Directory /var/www/html/public/bundles>
        FallbackResource disabled
    </Directory>
    SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
    ErrorLog /var/log/apache2/app_error.log
    CustomLog /var/log/apache2/app_access.log combined
</VirtualHost>
```

You also can modify the `Dockerfile` configuration to add or install some required packages that you need for your project.

All services are available in the `docker/docker-compose.yaml` file and environment variables in `docker/.env` file

## Build

Now, you can build the project with docker.

```
> make build
> make start
> make connect-to-app
```

```
> database: represent postgres database
> application: represent your application
> mailer: represent mailer service to send and receive email locally
```

## Available urls

All available urls with credentials for each service.

```
> application

url: http://localhost:5005
```

```
> postgres

To log, use these credentials:

Hostname/address: localhost
Port: 5448
Database: app_dev
Username: user
Password: password
```

```
> mailer

webmail url: http://localhost:8130
smtp server: smtp://mailer:2535
```